export interface MongoCart {
    _id: string;
    cartId: string;
    user: string;
    created_at: string;
    items: {
        quantity: number;
        variantId: string;
    }[];
    shop: string;
    updated_at: string;
};

export interface MongoOrder {
    _id: string;
    orders: {
        shop: string;
        order: string;
    }[];
    date: string;
    user: string;
    charge: string;
};

export interface MongoUser {
    _id: string;
    password: string;
    userSince: string;
    email: string;
    stripeCustomerId: string;
    firstName: string;
    lastName: string;
    shippingAddresses: {
        address1: string;
        address2: string;
        city: string;
        country: string;
        firstName: string;
        lastName: string;
        provinceCode: string;
        zip: string;
        default: any;
        _id: string;
    }[];
    token: string;
};

export interface MongoShop {
    _id: string;
    domain: string;
    accessToken: string;
    id: number;
    myshopifyDomain: string;
    connectAccountId: string;
};

export interface MongoShopifyOrder {
    _id: string;
    id: number;
    email: string;
    closed_at: null;
    created_at: string;
    updated_at: string;
    number: number;
    note: null;
    token: string;
    gateway: string;
    test: any;
    total_price: string;
    subtotal_price: string;
    total_weight: number;
    total_tax: string;
    taxes_included: any;
    currency: string;
    financial_status: string;
    confirmed: any;
    total_discounts: string;
    total_line_items_price: string;
    cart_token: null;
    buyer_accepts_marketing: any;
    name: string;
    referring_site: null;
    landing_site: null;
    cancelled_at: null;
    cancel_reason: null;
    total_price_usd: string;
    checkout_token: null;
    reference: null;
    user_id: null;
    location_id: null;
    source_identifier: null;
    source_url: null;
    processed_at: string;
    device_id: null;
    phone: null;
    customer_locale: null;
    app_id: number;
    browser_ip: null;
    landing_site_ref: null;
    order_number: number;
    discount_applications: any[];
    discount_codes: any[];
    note_attributes: any[];
    payment_gateway_names: any[];
    processing_method: string;
    checkout_id: null;
    source_name: string;
    fulfillment_status: string;
    tax_lines: any[];
    tags: string;
    contact_email: string;
    order_status_url: string;
    presentment_currency: string;
    total_line_items_price_set: {
        shop_money: {
            amount: string;
            currency_code: string;
        };
        presentment_money: {
            amount: string;
            currency_code: string;
        };
    };
    total_discounts_set: {
        shop_money: {
            amount: string;
            currency_code: string;
        };
        presentment_money: {
            amount: string;
            currency_code: string;
        };
    };
    total_shipping_price_set: {
        shop_money: {
            amount: string;
            currency_code: string;
        };
        presentment_money: {
            amount: string;
            currency_code: string;
        };
    };
    subtotal_price_set: {
        shop_money: {
            amount: string;
            currency_code: string;
        };
        presentment_money: {
            amount: string;
            currency_code: string;
        };
    };
    total_price_set: {
        shop_money: {
            amount: string;
            currency_code: string;
        };
        presentment_money: {
            amount: string;
            currency_code: string;
        };
    };
    total_tax_set: {
        shop_money: {
            amount: string;
            currency_code: string;
        };
        presentment_money: {
            amount: string;
            currency_code: string;
        };
    };
    line_items: {
        id: number;
        variant_id: number;
        title: string;
        quantity: number;
        sku: string;
        variant_title: string;
        vendor: string;
        fulfillment_service: string;
        product_id: number;
        requires_shipping: any;
        taxable: any;
        gift_card: any;
        name: string;
        variant_inventory_management: string;
        properties: any[];
        product_exists: any;
        fulfillable_quantity: number;
        grams: number;
        price: string;
        total_discount: string;
        fulfillment_status: string;
        price_set: {
            shop_money: {
                amount: string;
                currency_code: string;
            };
            presentment_money: {
                amount: string;
                currency_code: string;
            };
        };
        total_discount_set: {
            shop_money: {
                amount: string;
                currency_code: string;
            };
            presentment_money: {
                amount: string;
                currency_code: string;
            };
        };
        discount_allocations: any[];
        duties: any[];
        admin_graphql_api_id: string;
        tax_lines: any[];
    }[];
    fulfillments: {
        id: number;
        order_id: number;
        status: string;
        created_at: string;
        service: string;
        updated_at: string;
        tracking_company: null;
        shipment_status: null;
        location_id: number;
        line_items: {
            id: number;
            variant_id: number;
            title: string;
            quantity: number;
            sku: string;
            variant_title: string;
            vendor: string;
            fulfillment_service: string;
            product_id: number;
            requires_shipping: any;
            taxable: any;
            gift_card: any;
            name: string;
            variant_inventory_management: string;
            properties: any[];
            product_exists: any;
            fulfillable_quantity: number;
            grams: number;
            price: string;
            total_discount: string;
            fulfillment_status: string;
            price_set: {
                shop_money: {
                    amount: string;
                    currency_code: string;
                };
                presentment_money: {
                    amount: string;
                    currency_code: string;
                };
            };
            total_discount_set: {
                shop_money: {
                    amount: string;
                    currency_code: string;
                };
                presentment_money: {
                    amount: string;
                    currency_code: string;
                };
            };
            discount_allocations: any[];
            duties: any[];
            admin_graphql_api_id: string;
            tax_lines: any[];
        }[];
        tracking_number: null;
        tracking_numbers: any[];
        tracking_url: null;
        tracking_urls: any[];
        receipt: {
        
        };
        name: string;
        admin_graphql_api_id: string;
    }[];
    refunds: any[];
    total_tip_received: string;
    original_total_duties_set: null;
    current_total_duties_set: null;
    admin_graphql_api_id: string;
    shipping_lines: any[];
    customer: {
        id: number;
        email: string;
        accepts_marketing: any;
        created_at: string;
        updated_at: string;
        first_name: null;
        last_name: null;
        orders_count: number;
        state: string;
        total_spent: string;
        last_order_id: number;
        note: null;
        verified_email: any;
        multipass_identifier: null;
        tax_exempt: any;
        phone: null;
        tags: string;
        last_order_name: string;
        currency: string;
        accepts_marketing_updated_at: string;
        marketing_opt_in_level: null;
        tax_exemptions: any[];
        admin_graphql_api_id: string;
    };
};

export interface MongoDraftOrder {
    _id: string;
    shopify_id: number;
    shop: number;
};