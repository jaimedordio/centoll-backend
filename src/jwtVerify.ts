import dotenv from "dotenv";
import jwt from "jsonwebtoken";

dotenv.config();

export const jwtVerify = (token: string) => {
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET!);

    return {
      decoded,
      error: false,
      error_name: null,
      error_message: null,
    };
  } catch (err) {
    return {
      decoded: null,
      error: true,
      error_name: err.name,
      error_message: err.message,
    };
  }
};
