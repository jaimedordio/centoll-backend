import { IResolvers } from "graphql-tools";
import Cart from "./resolvers/Cart";
import CentollOrder from "./resolvers/CentollOrder";
import GroupedCart from "./resolvers/GroupedCart";
import Mutation from "./resolvers/Mutation";
import Query from "./resolvers/Query";
import stripe__PaymentMethod from "./resolvers/stripe__PaymentMethod";
import Subscription from "./resolvers/Subscription";
import User from "./resolvers/User";

const resolvers: IResolvers = {
  Query,
  Mutation,
  User,
  CentollOrder,
  Cart,
  Subscription,
  GroupedCart,
  stripe__PaymentMethod,
};

export default resolvers;
