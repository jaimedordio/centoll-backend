import "graphql-import-node";
import { makeExecutableSchema } from "graphql-tools";
import resolvers from "./resolver";
import * as customTypeDefs from "./schema/custom__schema.graphql";
import * as mainTypeDefs from "./schema/main__schema.graphql";
import * as shopifyTypeDefs from "./schema/shopify-schema.graphql";
import * as stripeTypeDefs from "./schema/stripe__schema.graphql";

const typeDefs = [
  mainTypeDefs,
  customTypeDefs,
  shopifyTypeDefs,
  stripeTypeDefs,
];

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

export default schema;
