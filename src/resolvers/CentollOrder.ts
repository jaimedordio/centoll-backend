import { MongoClient, ObjectID } from "mongodb";
import Shopify from "shopify-api-node";
import { CentollOrder, Order } from "../../gql";
import { MongoShop } from "../../mongo";
import { IGraphQLContext } from "../server";

const CentollOrder = {
  user: async (parent: any, args: any, ctx: IGraphQLContext, info: any) => {
    const userId = parent.user.toString();
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    const user_result = await usersCollection.findOne({
      _id: new ObjectID(userId),
    });

    return user_result;
  },

  orders: async (parent: any, args: any, ctx: IGraphQLContext, info: any) => {
    const orders: { shop: string; order: string }[] | undefined = parent.orders;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const finalOrders: Order[] = [];

    if (orders) {
      for (const orderMetadata of orders) {
        const shop_result: MongoShop | null = await shops_collection.findOne({
          myshopifyDomain: orderMetadata.shop,
        });

        console.log("\n");
        console.log("orderMetadata ", orderMetadata);
        console.log("shop_result ", shop_result);

        if (shop_result?.accessToken) {
          const shopify = new Shopify({
            accessToken: shop_result.accessToken,
            shopName: shop_result.myshopifyDomain,
          });

          try {
            const { order } = await shopify.graphql(
              `
                query($id: ID!) {
                  order(id: $id) {
                    id
                    confirmed
                    email
                    totalPriceSet {
                      presentmentMoney {
                        amount
                        currencyCode
                      }
                    }
                    lineItems(first: 100) {
                      edges {
                        node {
                          name
                          quantity
                        }
                      }
                    }
                    shippingAddress {
                      address1
                      address2
                    }
                  }
                }
              `,
              { id: orderMetadata.order }
            );

            console.log("ORDER ", order);
            console.log("\n");

            finalOrders.push(order);
          } catch (error) {
            console.log("Error fetching order: ", error);
          }
        }
      }
    }

    console.log("PRINTING ORDERS", finalOrders);
    console.log("\n");
    return finalOrders;
  },
};

export { CentollOrder as default };
