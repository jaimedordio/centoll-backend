import { MongoClient } from "mongodb";
import { IGraphQLContext } from "../server";

const User = {
  carts: async (parent: any, args: any, ctx: IGraphQLContext, info: any) => {
    const userId = parent._id.toString();
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const carts_collection = centoll_db.collection("carts");

    const cartResult = await carts_collection.find({ user: userId }).toArray();

    return cartResult;
  },

  orders: async (parent: any, args: any, ctx: IGraphQLContext, info: any) => {
    const userId = parent._id.toString();
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const ordersCollection = centoll_db.collection("orders");

    const ordersResult = await ordersCollection
      .find({ user: userId })
      .toArray();

    return ordersResult;
  },
};

export { User as default };
