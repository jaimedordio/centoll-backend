import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import { MongoClient, ObjectId, ObjectID } from "mongodb";
import Shopify from "shopify-api-node";
import Stripe from "stripe";
import { v4 as uuidv4 } from "uuid";
import {
  CartInput,
  DraftOrder,
  DraftOrderInput,
  MutationToAddShippingAddressArgs,
  MutationToChangeDefaultAddressArgs,
  MutationToChangeStripeDefaultSourceArgs,
  MutationToCompleteCheckoutArgs,
  MutationToCreateCustomerPaymentSourceArgs,
  MutationToCreateDraftOrderArgs,
  MutationToDeleteCartItemArgs,
  MutationToDeleteStripeSourceArgs,
  MutationToGetNewConnectAccountOnboardingUrlArgs,
  MutationToGetStripeConnectAccountLoginLinkArgs,
  MutationToLinkUserAndCartArgs,
  MutationToLoginArgs,
  MutationToRemoveCartArgs,
  MutationToRemoveShippingAddressArgs,
  MutationToSaveShopPropertiesArgs,
  MutationToSignupArgs,
  MutationToUpdateShopifyCartArgs,
  MutationToUpdateUserInformationArgs,
  MutationToUpdateUserPasswordArgs,
  Shop,
  ShopInput,
  UserError,
} from "../../gql";
import { MongoShop, MongoUser } from "../../mongo";
import { IGraphQLContext, stripe } from "../server";
import { log_req, removeEmptyFieldsFromObject } from "../utils";

dotenv.config();

const Mutation = {
  addShippingAddress: async (
    parent: any,
    args: MutationToAddShippingAddressArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shippingAddress } = args;

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const user_result: MongoUser | null = await usersCollection.findOne({
      email: user.decoded.email,
    });

    const newShippingAddress = {
      ...shippingAddress,
      default: false,
      _id: uuidv4(),
    };

    if (user_result && !user_result.shippingAddresses) {
      newShippingAddress.default = true;
    }

    const user_update_result = await usersCollection.findOneAndUpdate(
      { email: user.decoded.email },
      { $addToSet: { shippingAddresses: newShippingAddress } },
      { returnOriginal: false, upsert: true }
    );

    const new_user: MongoUser = user_update_result.value;

    console.log("new_user.shippingAddresses");
    console.log(new_user.shippingAddresses);

    return new_user.shippingAddresses;
  },

  removeShippingAddress: async (
    parent: any,
    args: MutationToRemoveShippingAddressArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shippingAddress } = args;

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const user_update_result = await usersCollection.findOneAndUpdate(
      { email: user.decoded.email },
      { $pull: { shippingAddresses: { _id: shippingAddress } } }
    );

    return Boolean(user_update_result.value);
  },

  changeDefaultAddress: async (
    parent: any,
    args: MutationToChangeDefaultAddressArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { id } = args;

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (!user || user.error) throw new Error("User not exists or not logged");

    try {
      const dbUser: MongoUser | null = await usersCollection.findOne({
        _id: new ObjectId(user.decoded._id),
      });

      if (!dbUser) throw new Error("No user found");

      const new_addresses = dbUser.shippingAddresses.map((address) => {
        let defaultAddress = false;

        if (address._id === id) {
          defaultAddress = true;
        }

        return { ...address, default: defaultAddress };
      });

      const new_user = await usersCollection.findOneAndUpdate(
        {
          _id: new ObjectId(user.decoded._id),
        },
        {
          $set: {
            shippingAddresses: new_addresses,
          },
        }
      );

      return new_user.value.shippingAddresses;
    } catch (error) {
      throw new Error(`Error updating customer: ${error}`);
    }
  },

  createStripeSource: async (
    parent: any,
    args: MutationToDeleteStripeSourceArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { sourceId } = args;

    const user = ctx.user;
    if (!user || user.error) throw new Error("User not exists or not logged");

    try {
      await stripe.customers.createSource(user.decoded.stripeCustomerId, {
        source: sourceId,
      });

      return true;
    } catch (error) {
      throw new Error(`Error creating customer source: ${error}`);
    }
  },

  changeStripeDefaultSource: async (
    parent: any,
    args: MutationToChangeStripeDefaultSourceArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { sourceId } = args;

    const user = ctx.user;
    if (!user || user.error) throw new Error("User not exists or not logged");

    try {
      await stripe.customers.update(user.decoded.stripeCustomerId, {
        default_source: sourceId,
      });

      return true;
    } catch (error) {
      throw new Error(`Error updating customer: ${error}`);
    }
  },

  deleteStripeSource: async (
    parent: any,
    args: MutationToDeleteStripeSourceArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { sourceId } = args;

    const user = ctx.user;
    if (!user || user.error) throw new Error("User not exists or not logged");

    try {
      await stripe.customers.deleteSource(
        user.decoded.stripeCustomerId,
        sourceId
      );

      return true;
    } catch (error) {
      throw new Error(`Error deleting customer source: ${error}`);
    }
  },

  signup: async (
    parent: any,
    args: MutationToSignupArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { firstName, lastName, email, password } = args;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (await usersCollection.findOne({ email })) {
      throw new Error(`Email "${email}" is not available.`);
    }

    const date = new Date();
    const userSince = date.toISOString();

    const stripeCustomer = await stripe.customers.create({
      name: `${firstName} ${lastName}`,
      email,
    });

    const user_result = await usersCollection.insertOne({
      firstName,
      lastName,
      email,
      password,
      userSince,
      stripeCustomerId: stripeCustomer.id,
    });

    return user_result.ops[0];
  },

  login: async (
    parent: any,
    args: MutationToLoginArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { email, password } = args;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    const userResult_beforeLogin = await usersCollection.findOne({
      email,
      password,
    });

    if (!userResult_beforeLogin) {
      throw new Error(`Wrong email or password.`);
    }

    const token = jwt.sign(
      { ...userResult_beforeLogin },
      process.env.JWT_SECRET!
      // { expiresIn: 20 } // 20 seconds
    );

    const userResult_afterLogin = await usersCollection.findOneAndUpdate(
      { email },
      { $set: { token } },
      { returnOriginal: false }
    );

    return userResult_afterLogin.value;
  },

  validateUserSession: async (
    parent: any,
    args: null,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (!user || user.error) return false;

    const user_result = await usersCollection.findOne({
      _id: new ObjectID(user.decoded._id),
    });

    if (!user_result) {
      return false;
    }

    return true;
  },

  logout: async (parent: any, args: null, ctx: IGraphQLContext, info: any) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (!user || user.error) return false;

    const user_result = await usersCollection.findOneAndUpdate(
      {
        _id: new ObjectID(user.decoded._id),
      },
      { $unset: { token: "" } }
    );

    if (!user_result) {
      return false;
    }

    return true;
  },

  saveShopProperties: async (
    parent: any,
    args: MutationToSaveShopPropertiesArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop }: { shop: ShopInput } = args;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const response = await shops_collection.findOneAndUpdate(
      { domain: shop.myshopifyDomain },
      {
        $set: { ...shop },
      },
      { upsert: true, returnOriginal: false }
    );

    return Boolean(response.ok);
  },

  updateShopifyCart: async (
    parent: any,
    args: MutationToUpdateShopifyCartArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { cart }: { cart: CartInput } = args;
    // const { pubsub } = ctx.mongo_client;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const carts_collection = centoll_db.collection("carts");

    const cartUpdateRes = await carts_collection.findOneAndUpdate(
      { cartId: cart.cartId },
      {
        $set: { ...cart },
      },
      { upsert: true, returnOriginal: false }
    );
    console.log("cartUpdateRes", cartUpdateRes);

    // pubsub.publish(user.decoded._id, {
    //   cartUpdate: cartUpdateRes,
    // });

    return cartUpdateRes.value;
  },

  updateUserInformation: async (
    parent: any,
    args: MutationToUpdateUserInformationArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { newInfo } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const users_collection = centoll_db.collection("users");

    const userUpdateRes = await users_collection.findOneAndUpdate(
      { _id: new ObjectID(user?.decoded._id) },
      {
        $set: { ...removeEmptyFieldsFromObject(newInfo) },
      },
      { upsert: true, returnOriginal: false }
    );

    return userUpdateRes.value;
  },

  updateUserPassword: async (
    parent: any,
    args: MutationToUpdateUserPasswordArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { oldPassword, newPassword } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const users_collection = centoll_db.collection("users");

    if (oldPassword !== user?.decoded.password) {
      throw new Error("Password does not match");
    }

    const userUpdateRes = await users_collection.findOneAndUpdate(
      { _id: new ObjectID(user?.decoded._id) },
      {
        $set: { password: newPassword },
      },
      { upsert: true, returnOriginal: false }
    );

    return userUpdateRes.value;
  },

  linkUserAndCart: async (
    parent: any,
    args: MutationToLinkUserAndCartArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { userId, cartId } = args;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const carts_collection = centoll_db.collection("carts");

    const linkRes = await carts_collection.findOneAndUpdate(
      { cartId },
      {
        $set: { user: userId },
      },
      { upsert: true, returnOriginal: false }
    );

    return Boolean(linkRes.ok);
  },

  removeCart: async (
    parent: any,
    args: MutationToRemoveCartArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { cartId } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");
    const carts_collection = centoll_db.collection("carts");

    if (!user || user.error) throw new Error("User not exists or not logged");

    if (
      !(await carts_collection.findOne({
        _id: new ObjectID(cartId),
        user: user.decoded._id,
      }))
    ) {
      throw new Error(`Cart ${cartId} isn't a user ${user.decoded._id} cart`);
    }

    await carts_collection.findOneAndDelete({ _id: new ObjectID(cartId) });

    return `Cart ${cartId} removed successfully.`;
  },

  deleteCartItem: async (
    parent: any,
    args: MutationToDeleteCartItemArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { cartId, productId } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const carts_collection = centoll_db.collection("carts");

    if (!user || user.error) throw new Error("User not exists or not logged");

    if (
      !(await carts_collection.findOne({
        _id: new ObjectID(cartId),
        user: user.decoded._id,
      }))
    ) {
      throw new Error(`Cart ${cartId} isn't a user ${user.decoded._id} cart`);
    }

    const date = new Date();
    const updated_at = date.toISOString();

    const userCartResult = await carts_collection.findOne({
      _id: new ObjectID(cartId),
      user: user.decoded._id,
    });

    userCartResult.items = userCartResult.items.filter(
      (item: { id: any }) => item.id != productId
    );

    await carts_collection.findOneAndUpdate(
      { _id: new ObjectID(cartId), user: user.decoded._id },
      { $set: { items: userCartResult.items, updated_at } }
    );

    return `Item ${productId} removed successfully.`;
  },

  getNewConnectAccountOnboardingUrl: async (
    parent: any,
    args: MutationToGetNewConnectAccountOnboardingUrlArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop } = args;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const shop_result: MongoShop | null = await shops_collection.findOne({
      domain: shop,
    });

    if (!shop_result) throw new Error(`Shop not found`);

    const shopify = new Shopify({
      accessToken: shop_result.accessToken,
      shopName: shop_result.myshopifyDomain,
    });

    const { shop: shop_details }: { shop: Shop } = await shopify.graphql(
      `
      query {
        shop {
          email
          myshopifyDomain
          name
          billingAddress {
            address1
            address2
            zip
            city
            countryCodeV2
            province
          }
        }
      }
    `
    );

    const account_list = await stripe.accounts.list();
    const account_exists = account_list.data.some(
      (account) => account.business_profile?.url === shop
    );

    if (account_exists) {
      throw new Error(
        `Connect account already exists for shop ${shop}. Get login link instead.`
      );
    }

    /**
     * Create account and prefill info
     */
    const account_data: Stripe.AccountCreateParams = {
      type: "express",
      country: shop_details.billingAddress.countryCodeV2,
      email: shop_details.email,
      business_profile: {
        url: shop_details.myshopifyDomain,
        name: shop_details.name,
        support_email: shop_details.email,
        support_address: {
          line1: shop_details.billingAddress.address1,
          line2: shop_details.billingAddress.address2,
          postal_code: shop_details.billingAddress.zip,
          city: shop_details.billingAddress.city,
          country: shop_details.billingAddress.countryCodeV2,
          state: shop_details.billingAddress.province,
        },
      },
    };

    const clean_account_data = removeEmptyFieldsFromObject(account_data);

    const created_account = await stripe.accounts.create({
      ...clean_account_data,
    });
    console.log("created_account");
    console.log(created_account);

    await shops_collection.findOneAndUpdate(
      { domain: shop },
      { $set: { connectAccountId: created_account.id } },
      { upsert: true }
    );

    /**
     * Create account link
     */
    const account_link = await stripe.accountLinks.create({
      account: created_account.id,
      type: "account_onboarding",
      return_url: `https://${shop}/admin/apps/sample-embedded-app-1827`,
      refresh_url: `https://${shop}/admin/apps/sample-embedded-app-1827`,
    });
    console.log("account_link");
    console.log(account_link);

    return account_link.url;
  },

  getStripeConnectAccountLoginLink: async (
    parent: any,
    args: MutationToGetStripeConnectAccountLoginLinkArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop } = args;

    const account_list = (await stripe.accounts.list()).data.filter(
      (account) => account.business_profile?.url === shop
    );

    const valid_account = account_list.find(
      (account) => account.charges_enabled
    );

    if (!valid_account) {
      throw new Error(
        `No valid connect account found for shop ${shop}. Get onboarding link instead.`
      );
    }

    const login_link = await stripe.accounts.createLoginLink(valid_account.id);

    return login_link.url;
  },

  createCustomerPaymentSource: async (
    parent: any,
    args: MutationToCreateCustomerPaymentSourceArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { cardToken } = args;

    const card = await stripe.customers.createSource("cus_IohwSMePHwGaVo", {
      source: cardToken,
    });

    console.log("card res", card);

    return card;
  },

  createDraftOrder: async (
    parent: any,
    args: MutationToCreateDraftOrderArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const {
      shop,
      input,
    }: // items, shippingAddress
    { shop: string; input: DraftOrderInput } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");
    const draft_orders_collection = centoll_db.collection("draft_orders");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const shop_result = await shops_collection.findOne({ domain: shop });

    if (!shop_result) throw new Error(`${shop} not found`);
    if (!shop_result.accessToken) {
      throw new Error(`${shop} has not an access token`);
    }

    const shopify = new Shopify({
      accessToken: shop_result.accessToken,
      shopName: shop_result.domain,
    });

    const draft_order = await shopify.draftOrder.create({
      email: user.decoded.email,
      input,
    });
    // .then(async (res) => {
    console.log(
      `Draft Order ${draft_order.name} created on ${shop_result.domain}`
    );

    // const insert_draft_order_result = await draft_orders_collection.insertOne(
    //   {
    //     user_id: user.decoded._id,
    //     shop: shop_result.id,
    //     shopify_id: res.id,
    //   }
    // );

    // return {
    //   shop: shop_result,
    //   order: res,
    // id: insert_draft_order_result.ops[0]._id,
    //   };
    // })
    // .catch((err) => {
    //   throw new Error(`Error creating Draft Order on ${shop}: ${err}`);
    // });

    return draft_order.id;
  },

  completeCheckout: async (
    parent: any,
    args: MutationToCompleteCheckoutArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { data, paymentMethod } = args;

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");
    const draft_orders_collection = centoll_db.collection("draft_orders");
    const orders_collection = centoll_db.collection("orders");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const shopify_clients: {
      [shop: string]: { client: Shopify; shop: MongoShop };
    } = {};

    /**
     * For each shop in data create shopify client
     */
    for (const shop_order_data of data) {
      const shop_result: MongoShop | null = await shops_collection.findOne({
        myshopifyDomain: shop_order_data.shop,
      });

      if (!shop_result) throw new Error(`${shop_order_data.shop} not found`);
      if (!shop_result.accessToken) {
        throw new Error(`${shop_order_data.shop} has not an access token`);
      }
      if (!shop_result.connectAccountId) {
        throw new Error(
          `${shop_order_data.shop} has not a Stripe connected account`
        );
      }

      shopify_clients[shop_result.myshopifyDomain] = {
        client: new Shopify({
          accessToken: shop_result.accessToken,
          shopName: shop_result.myshopifyDomain,
        }),
        shop: shop_result,
      };
    }

    const draft_orders_data_promises: Promise<{
      shop: MongoShop;
      draftOrder: DraftOrder;
    }>[] = [];
    /**
     * For each shop in data create draft order
     */
    for (const shop_order_data of data) {
      const shopify_client = shopify_clients[shop_order_data.shop];

      if (shopify_client) {
        /**
         * Create the Draft Order
         */
        const input: DraftOrderInput = {
          email: user.decoded.email,
          lineItems: shop_order_data.items,
          shippingAddress: shop_order_data.shippingAddress,
          shippingLine: { shippingRateHandle: shop_order_data.shippingLine },
        };

        draft_orders_data_promises.push(
          shopify_client.client
            .graphql(
              `
            mutation ($input: DraftOrderInput!) {
              draftOrderCreate(input: $input) {
                draftOrder {
                  id
                  name
                  subtotalPrice
                  totalPrice
                  totalTax
                  totalShippingPrice
                }
                userErrors {
                  field
                  message
                }
              }
            }`,
              {
                input,
              }
            )
            .then(
              async ({
                draftOrderCreate,
              }: {
                draftOrderCreate: {
                  draftOrder: DraftOrder;
                  userErrors: UserError[];
                };
              }) => {
                if (draftOrderCreate.userErrors.length) {
                  throw new Error(
                    `Error creating Draft Order on ${shop_order_data.shop}: ${draftOrderCreate.userErrors}`
                  );
                }

                console.log(
                  `Draft Order ${draftOrderCreate.draftOrder.name} created on ${shop_order_data.shop}`
                );

                return {
                  shop: shopify_client.shop,
                  draftOrder: draftOrderCreate.draftOrder,
                };
              }
            )
            .catch((err) => {
              throw new Error(
                `Error creating Draft Order on ${shop_order_data.shop}: ${err}`
              );
            })
        );
      } else {
        throw new Error(`No shopify client for ${shop_order_data.shop}`);
      }
    }

    /**
     * Resolve Draft Order creations
     */
    const draft_orders_data: {
      shop: MongoShop;
      draftOrder: DraftOrder;
    }[] = await Promise.all(draft_orders_data_promises);

    /**
     * Calculate amounts
     */
    const orders_total_price = draft_orders_data
      .map((draft_order) => parseFloat(draft_order.draftOrder.totalPrice))
      .reduce((accumulator, current) => accumulator + current);

    let transfer_group = "";
    draft_orders_data.forEach(
      (order) =>
        (transfer_group += `${order.shop.myshopifyDomain},${order.draftOrder.id};`)
    );

    /**
     * Charge total to customer
     */
    const customer_charge = await stripe.charges.create({
      amount: Math.ceil(orders_total_price * 100),
      currency: "eur",
      customer: user.decoded.stripeCustomerId,
      source: paymentMethod,
      transfer_group,
      expand: ["balance_transaction"],
    });

    // @ts-ignore
    const charged_amount = customer_charge.balance_transaction?.amount;
    // @ts-ignore
    const charged_fee = customer_charge.balance_transaction?.fee;

    /**
     * Transfer each order amount to connected accounts
     */
    try {
      for (const draft_order of draft_orders_data) {
        // Total price + taxes
        const amount = Math.ceil(
          parseFloat(draft_order.draftOrder.totalPrice) * 100
        );

        // Centoll fee
        const centollFee = Math.ceil(0.1 * amount);

        // Stripe fee splitted by percentage of total amount
        const splittedStripeFee = Math.ceil(
          charged_fee * (amount / charged_amount)
        );

        // Transfer to connected account
        await stripe.transfers.create({
          amount: amount - centollFee - splittedStripeFee,
          currency: "eur",
          destination: draft_order.shop.connectAccountId!,
          transfer_group,
          source_transaction: customer_charge.id,
        });
      }
    } catch (error) {
      throw new Error(
        `Error transfering amounts to connected accounts: ${error}`
      );
    }

    const complete_draft_orders_data_promises: Promise<{
      shop: MongoShop;
      draftOrder: DraftOrder;
    }>[] = [];
    /**
     * For each shop in draft orders data complete draft orders
     */
    for (const draft_order of draft_orders_data) {
      const shopify_client = shopify_clients[draft_order.shop.myshopifyDomain];

      if (shopify_client) {
        /**
         * Complete the Draft Order
         */
        complete_draft_orders_data_promises.push(
          shopify_client.client
            .graphql(
              `
            mutation ($id: ID!) {
              draftOrderComplete(id: $id)  {
                draftOrder {
                  id
                  order {
                    name
                    id
                  }
                }
                userErrors {
                  field
                  message
                }
              }
            }`,
              {
                id: draft_order.draftOrder.id,
              }
            )
            .then(
              async ({
                draftOrderComplete,
              }: {
                draftOrderComplete: {
                  draftOrder: DraftOrder;
                  userErrors: UserError[];
                };
              }) => {
                if (draftOrderComplete.userErrors.length) {
                  throw new Error(
                    `Error completing Draft Order on ${draft_order.shop.myshopifyDomain}: ${draftOrderComplete.userErrors}`
                  );
                }

                console.log(
                  `Draft Order ${draft_order.draftOrder.name} completed ${draft_order.shop.myshopifyDomain}`
                );

                return {
                  shop: shopify_client.shop,
                  draftOrder: draftOrderComplete.draftOrder,
                };
              }
            )
            .catch((err) => {
              throw new Error(
                `Error completing Draft Order on ${draft_order.shop.myshopifyDomain}: ${err}`
              );
            })
        );
      } else {
        throw new Error(
          `No shopify client for ${draft_order.shop.myshopifyDomain}`
        );
      }
    }

    /**
     * Resolve Draft Order completions
     */
    const complete_draft_orders_data: {
      shop: MongoShop;
      draftOrder: DraftOrder;
    }[] = await Promise.all(complete_draft_orders_data_promises);

    const orders = complete_draft_orders_data.map(
      (draft_order) => draft_order.draftOrder.order
    );

    /**
     * Save orders on DB
     */
    await orders_collection.insertOne({
      orders: complete_draft_orders_data.map((order) => ({
        shop: order.shop.myshopifyDomain,
        order: order.draftOrder.order?.id,
      })),
      date: new Date().toISOString(),
      user: user.decoded._id,
      charge: customer_charge.id,
    });

    return orders;
  },
};

export default Mutation;
