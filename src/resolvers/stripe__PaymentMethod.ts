import Stripe from "stripe";
import { stripe__PaymentMethod } from "../../gql";
import { IGraphQLContext, stripe } from "../server";

const stripe__PaymentMethod = {
  default: async (
    parent: stripe__PaymentMethod,
    args: any,
    ctx: IGraphQLContext,
    info: any
  ) => {
    // @ts-ignore
    const customer: Stripe.Response<Stripe.Customer> = await stripe.customers.retrieve(
      ctx.user?.decoded.stripeCustomerId!
    );

    const customerDefaultSource = customer.default_source;

    if (customerDefaultSource === parent.id) {
      return true;
    }

    return false;
  },
};

export default stripe__PaymentMethod;
