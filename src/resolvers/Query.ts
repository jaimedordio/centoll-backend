import axios from "axios";
import { MongoClient, ObjectID } from "mongodb";
import Shopify from "shopify-api-node";
import {
  Cart,
  DraftOrderLineItemInput,
  QueryToCheckMerchantActiveInstallArgs,
  QueryToCheckValidStripeConnectAccountArgs,
  QueryToDraftOrderArgs,
  QueryToDraftOrderCalculateArgs,
  QueryToGetOrderArgs,
  QueryToGetShopAccessTokenArgs,
  Shop,
  User,
} from "../../gql";
import { MongoCart, MongoShop } from "../../mongo";
import { IGraphQLContext, stripe } from "../server";
import { log_req } from "../utils";

const Query = {
  /**
   * Get carts spreaded by id (as saved in DB)
   */
  getCart: async (parent: any, args: null, ctx: IGraphQLContext, info: any) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const carts_collection = centoll_db.collection("carts");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const carts_result: Cart[] = await carts_collection
      .find({
        user: user.decoded._id,
      })
      .toArray();

    return carts_result;
  },

  /**
   * Get carts gruped by domain
   */
  getGroupedCart: async (
    parent: any,
    args: null,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const carts_collection = centoll_db.collection("carts");

    console.log("user", user);
    if (!user || user.error) throw new Error("User not exists or not logged");

    const carts_result: MongoCart[] = await carts_collection
      .find({
        user: user.decoded._id,
      })
      .toArray();

    console.log("carts_result");
    console.log(carts_result);

    const domains = carts_result
      .map((c) => c.shop)
      .filter((elem, index, self) => index === self.indexOf(elem));

    const grouped_carts: {
      shop: string;
      items: DraftOrderLineItemInput[];
    }[] = [];

    for (const domain of domains) {
      const domain_carts_items: DraftOrderLineItemInput[] = [];

      /* Filter domain carts and push items to same array */
      carts_result
        .filter((cart) => cart.shop === domain)
        .forEach((cart) => {
          if (cart.items) domain_carts_items.push(...cart.items);
        });

      grouped_carts.push({ shop: domain, items: domain_carts_items });
    }

    console.log("grouped_carts");
    console.log(grouped_carts);

    return grouped_carts;
  },

  getUser: async (parent: any, args: null, ctx: IGraphQLContext, info: any) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const user_result: User | null = await usersCollection.findOne({
      _id: new ObjectID(user.decoded._id),
    });

    return user_result;
  },

  getShopAccessToken: async (
    parent: any,
    args: QueryToGetShopAccessTokenArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop } = args;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const shop_result = await shops_collection.findOne({ domain: shop });

    if (!shop_result) {
      throw new Error("Not existing token for that shop.");
    }

    return shop_result.accessToken;
  },

  getOrder: async (
    parent: any,
    args: QueryToGetOrderArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { id } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("orders");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const orderResult = await usersCollection.findOne({
      id,
    });

    return orderResult;
  },

  getOrders: async (
    parent: any,
    args: null,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const ordersCollection = centoll_db.collection("orders");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const ordersResult = await ordersCollection
      .find({
        user: user.decoded._id,
      })
      .toArray();

    console.log(
      "ordersResult",
      ordersResult.map((order) => order.orders)
    );

    return ordersResult.reverse();
  },

  getUserPaymentMethods: async (
    parent: any,
    args: null,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    if (!user || user.error) throw new Error("User not exists or not logged");

    const paymentMethods = await stripe.paymentMethods.list({
      customer: user.decoded.stripeCustomerId,
      type: "card",
    });

    // console.log("paymentMethods Res");
    // paymentMethods.data.forEach((paymentMethod) => console.log(paymentMethod));
    // console.log(paymentMethods);

    return paymentMethods.data;
  },

  getUserShippingAddresses: async (
    parent: any,
    args: null,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const user_result: User | null = await usersCollection.findOne({
      _id: new ObjectID(user.decoded._id),
    });

    return user_result?.shippingAddresses;
  },

  draftOrderCalculate: async (
    parent: any,
    args: QueryToDraftOrderCalculateArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop, input } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    if (!user || user.error) throw new Error("User not exists or not logged");

    const shop_result: MongoShop | null = await shops_collection.findOne({
      myshopifyDomain: shop,
    });

    if (!shop_result) throw new Error(`${shop} not found`);
    if (!shop_result.accessToken) {
      throw new Error(`${shop} has not an access token`);
    }

    const shopify = new Shopify({
      accessToken: shop_result.accessToken,
      shopName: shop_result.myshopifyDomain,
    });

    const draftOrderCalculateRes = await shopify.graphql(
      `
        mutation ($input: DraftOrderInput!) {
          draftOrderCalculate(input: $input) {
            calculatedDraftOrder {
              subtotalPrice
              totalPrice
              totalShippingPrice
              totalTax
              availableShippingRates {
                handle
                title
                price {
                  amount
                  currencyCode
                }
              }
            }
            userErrors {
              field
              message
            }
          }
        }
      `,
      {
        input,
      }
    );

    if (draftOrderCalculateRes.draftOrderCalculate.userErrors.length) {
      throw new Error(
        `Error fetching shipping rates: ${JSON.stringify(
          draftOrderCalculateRes.draftOrderCalculate.userErrors
        )}`
      );
    }

    return draftOrderCalculateRes.draftOrderCalculate.calculatedDraftOrder;
  },

  checkMerchantActiveInstall: async (
    parent: any,
    args: QueryToCheckMerchantActiveInstallArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop } = args;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const shop_result: Shop | null = await shops_collection.findOne({
      myshopifyDomain: shop,
    });

    if (shop_result?.accessToken) {
      try {
        const shop_response = await axios.get(
          `https://${shop}/admin/api/2020-07/shop.json`,
          {
            headers: {
              "X-Shopify-Access-Token": shop_result?.accessToken,
            },
          }
        );

        return true;
      } catch (error) {
        return false;
      }
    }

    return false;
  },

  checkValidStripeConnectAccount: async (
    parent: any,
    args: QueryToCheckValidStripeConnectAccountArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop } = args;

    const account_list = (await stripe.accounts.list()).data.filter(
      (account) => account.business_profile?.url === shop
    );

    const valid_account = account_list.some(
      (account) => account.charges_enabled
    );

    return valid_account;
  },

  draftOrder: async (
    parent: any,
    args: QueryToDraftOrderArgs,
    ctx: IGraphQLContext,
    info: any
  ) => {
    log_req(ctx, info);

    const { shop } = args;
    const mongo_client: MongoClient = ctx.mongo_client;
    const user = ctx.user;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const shop_result: Shop | null = await shops_collection.findOne({
      domain: shop,
    });

    console.log("stripe charge");
    console.log(
      JSON.stringify(
        await stripe.charges.retrieve("ch_1IcUABK19feAehoJlikOPXpL", {
          expand: ["balance_transaction"],
        })
      )
    );
    return true;

    // if (shop_result?.accessToken) {
    //   try {
    //     const items = [
    //       {
    //         variant_id: "31349751414887",
    //         quantity: 1,
    //       },
    //     ];

    //     const shipping_address = {
    //       address1: "Avenida Reina Victoria, 29",
    //       address2: "Apt 2",
    //       city: "Madrid",
    //       company: null,
    //       country: "Spain",
    //       first_name: "Jaime",
    //       last_name: "Dordio",
    //       phone: "+34626008861",
    //       province: "Madrid",
    //       zip: "28003",
    //       country_code: "ES",
    //       province_code: "MD",
    //     };

    //     /**
    //      * Update the Draft Order
    //      */
    //     const update_draft_order_response = await axios.put(
    //       `https://${shop}/admin/api/2020-10/draft_orders/839419232359.json`,
    //       {
    //         draft_order: {
    //           items,
    //           use_customer_default_address: true,
    //           email: user.decoded.email,
    //         },
    //       },
    //       {
    //         headers: {
    //           "X-Shopify-Access-Token": shop_result?.accessToken,
    //         },
    //       }
    //     );

    //     console.log("update_draft_order_response", update_draft_order_response);

    //     const draft_order_id = update_draft_order_response.data.draft_order.id;

    //     /**
    //      * Get the Draft Order info
    //      */
    //     const draft_order_response = await axios.get(
    //       `https://${shop}/admin/api/2021-01/draft_orders/${draft_order_id}.json`,
    //       {
    //         headers: {
    //           "X-Shopify-Access-Token": shop_result?.accessToken,
    //         },
    //       }
    //     );

    //     console.log("draft_order_response", draft_order_response.data);

    //     const splitted_url: string[] = draft_order_response.data.draft_order.invoice_url.split(
    //       "/"
    //     );

    //     const checkout_token = splitted_url[splitted_url.length - 1];
    //     console.log("checkout_token", checkout_token);

    //     /**
    //      * Create checkout for fetching Shipping Rates
    //      */
    //     const create_checkout_res = await axios.post(
    //       `https://${shop}/admin/api/2021-01/checkouts.json`,
    //       {
    //         checkout: {
    //           line_items: items,
    //           email: user.decoded.email,
    //           shipping_address,
    //         },
    //       },
    //       {
    //         headers: {
    //           "X-Shopify-Access-Token": shop_result.accessToken,
    //         },
    //       }
    //     );

    //     console.log("create_checkout_res");
    //     console.log(create_checkout_res);

    //     /**
    //      * Get Shipping Rates
    //      */
    //     const shipping_rates_res = await axios.get(
    //       `https://${shop}/admin/api/2020-10/checkouts/${create_checkout_res.data.checkout.token}/shipping_rates.json`,
    //       {
    //         headers: {
    //           "X-Shopify-Access-Token": shop_result.accessToken,
    //         },
    //       }
    //     );

    //     console.log("shipping_rates_res.data.shipping_rates");
    //     console.log(shipping_rates_res.data.shipping_rates);

    //     return true;
    //   } catch (error) {
    //     console.error(`Error on draft order: ${error}`);
    //     throw new Error(`Error on draft order: ${error}`);
    //   }
    // }

    return false;
  },
};

export default Query;
