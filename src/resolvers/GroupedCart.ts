import { MongoClient } from "mongodb";
import Shopify from "shopify-api-node";
import { DraftOrderLineItemInput, ProductVariant, Shop } from "../../gql";
import { IGraphQLContext } from "../server";

interface IGroupedCartParent {
  shop: string;
  items: DraftOrderLineItemInput[];
}

const GroupedCart = {
  shop: async (
    parent: IGroupedCartParent,
    args: any,
    ctx: IGraphQLContext,
    info: any
  ) => {
    const shop_domain = parent.shop;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const shop_result = await shops_collection.findOne({
      myshopifyDomain: shop_domain,
    });

    if (shop_result?.accessToken) {
      const shopify = new Shopify({
        accessToken: shop_result.accessToken,
        shopName: shop_domain,
      });

      const { shop }: { shop: Shop } = await shopify.graphql(`
        query {
          shop {
            id
            name
            myshopifyDomain
            primaryDomain {
              url
            }
          }
        }
      `);

      return shop;
    }

    return {};
  },

  items: async (
    parent: IGroupedCartParent,
    args: any,
    ctx: IGraphQLContext,
    info: any
  ) => {
    const shop_domain = parent.shop;
    const items = parent.items;
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const shop_result = await shops_collection.findOne({
      myshopifyDomain: shop_domain,
    });

    const items_promises: Promise<ProductVariant>[] = [];

    if (shop_result?.accessToken) {
      const shopify = new Shopify({
        accessToken: shop_result.accessToken,
        shopName: shop_domain,
      });

      items.forEach((item) => {
        const productVariantRes: Promise<ProductVariant> = shopify
          .graphql(
            `query ($id: ID!) {
              productVariant(id: $id) {
                displayName
                id
                title
                inventoryQuantity
                price
                presentmentPrices(presentmentCurrencies: [EUR, USD], first: 10) {
                  edges {
                    node {
                      compareAtPrice {
                        amount
                        currencyCode
                      }
                      price {
                        amount
                        currencyCode
                      }
                    }
                  }
                }
                product {
                  images(first: 5) {
                    edges {
                      node {
                        originalSrc
                      }
                    }
                  }
                }
              }
            }`,
            { id: `gid://shopify/ProductVariant/${item.variantId}` }
          )
          .then((res) => ({ ...res.productVariant, quantity: item.quantity }))
          .catch((err) => {
            throw new Error(`Error fetching product variant info: ${err}`);
          });

        items_promises.push(productVariantRes);
      });

      return await Promise.all(items_promises);
    }

    return [];
  },
};

export default GroupedCart;
