import { MongoClient, ObjectID } from "mongodb";
import { IGraphQLContext } from "../server";

const Cart = {
  shop: async (parent: any, args: any, ctx: IGraphQLContext, info: any) => {
    const shop = parent.shop;
    console.log("shopify__ShopifyCart parent", parent);

    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const shops_collection = centoll_db.collection("shops");

    const shop_result = await shops_collection.findOne({
      domain: shop,
    });

    return shop_result;
  },

  user: async (parent: any, args: any, ctx: IGraphQLContext, info: any) => {
    const userId = parent.user.toString();
    const mongo_client: MongoClient = ctx.mongo_client;

    const centoll_db = mongo_client.db("centoll");
    const usersCollection = centoll_db.collection("users");

    const user_result = await usersCollection.findOne({
      _id: new ObjectID(userId),
    });

    return user_result;
  },
};

export { Cart as default };
