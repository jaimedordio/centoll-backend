import * as Sentry from "@sentry/node";
import { ApolloServerPluginSchemaReporting } from "apollo-server-core";
import { ApolloServer } from "apollo-server-express";
import chalk from "chalk";
import compression from "compression";
import dotenv from "dotenv";
import express, { Application } from "express";
import { generateTypeScriptTypes } from "graphql-schema-typescript";
import { MongoClient } from "mongodb";
import Stripe from "stripe";
import { User } from "../gql";
import { jwtVerify } from "./jwtVerify";
import schema from "./schema";

dotenv.config();

const usr = process.env.MONGO_DB_USERNAME!;
const pwd = process.env.MONGO_DB_PASSWORD!;
const url = process.env.MONGO_DB_URL!;

export const stripe = new Stripe(process.env.STRIPE_API_KEY!, {
  apiVersion: "2020-08-27",
});

export interface IGraphQLContext {
  mongo_client: MongoClient;
  user?: {
    decoded: User;
    error: boolean;
    error_name?: string;
    error_message?: string;
  };
}

const connectToDb = async (
  mongoUsr: string,
  mongoPwd: string,
  mongoUrl: string
) => {
  const uri = `mongodb+srv://${mongoUsr}:${mongoPwd}@${mongoUrl}`;
  const client: MongoClient = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await client.connect();
  return client;
};

const runApp = async () => {
  // const pubsub = new PubSub();
  const mongo_client: MongoClient = await connectToDb(usr, pwd, url);
  console.log(chalk.green(`\n💾 Connected to Mongo DB\n`));

  /**
   * ----------------------
   * GENERATE TS TYPES
   * ----------------------
   */
  generateTypeScriptTypes(schema, "gql.d.ts", { typePrefix: "" })
    .then(() => {
      console.log(chalk.cyanBright(`\n🛠  GraphQL TS types generated\n`));
    })
    .catch((err) => {
      console.error("Error creating TS Types: ", err);
    });

  /**
   * ----------------------
   * SAVE SHOPIFY GQL SCHEMA
   * ----------------------
   */
  // const shop_domain_to_fetch = "express-shop-developing.myshopify.com";
  // const shop_result = await mongo_client
  //   .db("centoll")
  //   .collection("shops")
  //   .findOne({ domain: shop_domain_to_fetch });

  // if (!shop_result) {
  //   throw new Error("Not shop found for fetching Shopify GQL Schema");
  // } else {
  //   await getShopifyGraphQLSchema(shop_result.domain, shop_result.accessToken);
  // }

  try {
    /**
     * ----------------------
     * Set up Apollo Server - GraphQL
     * ----------------------
     */
    const server = new ApolloServer({
      schema,
      context: async ({ req, connection }) => {
        if (connection) {
          const token = connection.context.authorization || "";
          const user = jwtVerify(token.split(" ")[1]);

          return { user, mongo_client };
        }

        if (req.headers && req.headers.authorization) {
          const token = req.headers.authorization || "";
          const user = jwtVerify(token.split(" ")[1]);

          return { user, mongo_client };
        }

        return { mongo_client };
      },
      introspection: true,
      playground: true,
      plugins: [ApolloServerPluginSchemaReporting()],
    });

    /** Express App */
    const app: Application = express();

    /** Initialize Sentry */
    Sentry.init({
      dsn:
        "https://9523053ac9144d028d868b1cf5f96961@o534327.ingest.sentry.io/5653631",
    });

    /**
     * ----------------------
     * MIDDLEWARES
     * ----------------------
     */

    /** Sentry Request Handler */
    app.use(Sentry.Handlers.requestHandler());

    /** Headers Middleware */
    // app.use((req, res, next) => {
    //   res.header("Access-Control-Allow-Origin", "*");
    //   res.header(
    //     "Access-Control-Allow-Headers",
    //     "Origin, X-Requested-With, Content-Type, Accept"
    //   );
    //   next();
    // });

    /** CORS Middleware */
    // app.use("*", cors());

    /** Compression Middleware */
    app.use(compression());

    /** GraphQL Middleware */
    // @ts-ignore
    server.applyMiddleware({ app, path: "/graphql" }); // TS fix needed

    /** Sentry Error Handler */
    app.use(Sentry.Handlers.errorHandler());

    /**
     * ----------------------
     * SERVER START
     * ----------------------
     */

    const port = process.env.PORT || 8001;

    app.listen(port, () => {
      console.log(chalk.cyan(`\n🚀 Server ready on port ${port}\n`));
    });
  } catch (e) {
    console.info(e);
    mongo_client.close();
  }
};

runApp();
