import axios from "axios";
import chalk from "chalk";
import fs from "fs";
import { buildClientSchema, getIntrospectionQuery, printSchema } from "graphql";
import path from "path";
import { IGraphQLContext } from "./server";

/**
 * Cleans falsy values from Object
 *
 * @param obj {Object} - Raw object
 * @returns {Object} Clean object
 */
export const removeEmptyFieldsFromObject = (obj: object): object => {
  return Object.entries(obj)
    .filter(([_, v]) => {
      if (v) return v;
    })
    .reduce(
      (acc, [k, v]) => ({
        ...acc,
        [k]: v === Object(v) ? removeEmptyFieldsFromObject(v) : v,
      }),
      {}
    );
};

/**
 * Log request
 *
 * @param ctx {Object} - Request context object
 * @param info {Object} - Request info object
 */
export const log_req = (ctx: IGraphQLContext, info: any): void => {
  const today = new Date();
  const time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}.${today.getMilliseconds()}`;

  console.log(
    chalk.yellow(`\n# [${time}]`),
    "REQUEST MADE TO ",
    chalk.yellow(info.fieldName)
  );

  console.log(chalk.yellow("Operation type: "), info.operation.operation);

  if (ctx.user && !ctx.user.error) {
    console.log(chalk.yellow(`User:`));
    Object.keys(ctx.user.decoded).forEach((key) =>
      // @ts-ignore
      console.log(chalk.yellow(`  ${key}: `), ctx.user!.decoded[key])
    );

    // console.log(chalk.yellow(`  id: `), ctx.user.decoded?._id);
    // console.log(chalk.yellow(`  email: `), ctx.user.decoded?.email);
  }

  console.log(chalk.yellow(`Arguments:`));
  console.log(info.variableValues);

  console.log(chalk.yellow("##\n"));
};

/**
 * Rename a list of object keys
 *
 * @param { IRenameKeys } obj - Original object.
 * @param { IRenameKeys } newKeys - Original object.
 *
 * @returns { number } Subtotal
 */
interface IObject {
  [key: string]: any;
}

export const renameKeys = (obj: IObject, newKeys: IObject): IObject => {
  const keyValues = Object.keys(obj).map((key) => {
    const newKey = newKeys[key] || key;
    return { [newKey]: obj[key] };
  });
  return Object.assign({}, ...keyValues);
};

/**
 * Transform object keys from snake_case to camelCase
 *
 * @param input {String} - String to transform
 * @returns {Object} Transformed string
 */
export const snakeCaseToCamelCase = (input: string): string => {
  return input.replace(/([-_][a-z])/gi, ($1) => {
    return $1.toUpperCase().replace("-", "").replace("_", "");
  });
};

const isObject = (obj: object) =>
  obj === Object(obj) && !Array.isArray(obj) && typeof obj !== "function";

export const snakeCaseKeysToCamelCase = (obj: IObject): IObject => {
  if (isObject(obj)) {
    const n: IObject = {};

    Object.keys(obj).forEach((k) => {
      n[snakeCaseToCamelCase(k)] = obj[k];
    });

    return n;
  } else if (Array.isArray(obj)) {
    return obj.map((i) => {
      return snakeCaseKeysToCamelCase(i);
    });
  }

  return obj;
};

/**
 * Get Shopify GraphQL API Schema and save it to a file
 *
 * @param shop { String } - Shop domain for fetching GraphQL schema
 * @param accessToken { String } - Shop access token
 */
export const getShopifyGraphQLSchema = async (
  shop: string,
  accessToken: string
) => {
  await axios
    .post(
      `https://${shop}/admin/api/2021-04/graphql.json`,
      {
        query: getIntrospectionQuery(),
      },
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "X-Shopify-Access-Token": accessToken,
        },
      }
    )
    .then((schemaJSON) =>
      printSchema(
        buildClientSchema(schemaJSON.data.data, { assumeValid: true })
      )
    )
    .then((clientSchema) =>
      fs.writeFileSync(
        path.join(__dirname, "./schema", "shopify-schema.graphql"),
        clientSchema
      )
    )
    .catch((err) => {
      throw new Error(`Error fetching Shopify GraphQL Schema: ${err}`);
    });
};
